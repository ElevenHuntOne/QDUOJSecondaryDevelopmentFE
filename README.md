# QDUOJSecondaryDevelopmentFE

青岛大学OJ前端开源地址：[OnlineJudgeFE](https://github.com/QingdaoU/OnlineJudgeFE)

本前端使用方式：使用[OnlineJudgeDeploy](https://github.com/QingdaoU/OnlineJudgeDeploy/tree/2.0)中提到的方法构建好青岛OJ，然后将本项目build好后放到部署好的项目中。**注意**：我二次开发后的前端部分功能需要依托于我[二次开发的后端](https://gitee.com/ElevenHuntOne/QDUOJSecondaryDevelopment)。

build操作指南：

将`dist`文件夹复制到服务器上某个目录下，比如`/root/OnlineJudgeDeploy/app/dist`，然后修改`docker-compose.yml`，在`oj-backend`模块中的`volumes`中增加一行

` - /root/OnlineJudgeDeploy/app/dist:/app/dist`（冒号前面的请修改为实际的路径），然后`docker-compose up -d`即可。

**注意**：这种修改方式将覆盖容器内的前端文件，未来发布新版本前端的时候，请自行使用相同的方式更新。

>### A multiple pages app built for [OnlineJudge](https://gitee.com/ElevenHuntOne/QDUOJSecondaryDevelopment). [Demo](http://oj.xiaoao.space)

## Features

+ Webpack3 multiple pages with bundle size optimization
+ Easy use simditor & Nice codemirror editor
+ Amazing charting and visualization(echarts)
+ User-friendly operation
+ Quite beautiful：)

## Get Started

Install nodejs **v8.12.0** first.

## 本地测试方法

### Linux

```bash
npm install
# we use webpack DllReference to decrease the build time,
# this command only needs execute once unless you upgrade the package in build/webpack.dll.conf.js
export NODE_ENV=development 
npm run build:dll

# the dev-server will set proxy table to your backend
export TARGET=http://Your-backend

# serve with hot reload at localhost:8080
npm run dev
```
### Windows

```bash
npm install
# we use webpack DllReference to decrease the build time,
# this command only needs execute once unless you upgrade the package in build/webpack.dll.conf.js
set NODE_ENV=development 
npm run build:dll

# the dev-server will set proxy table to your backend
set TARGET=http://Your-backend

# serve with hot reload at localhost:8080
npm run dev
```

## Quick Modify

### 修改公告

![](./public/修改公告.gif)

### 修改题目

![](./public/修改题目.gif)

### 修改比赛

![](./public/修改比赛.gif)

### 复制提交

![](./public/复制提交.gif)

## Sorted Submission

![](./public/提交排序与筛选.gif)

## More Theme(developing...)

（不能直接用原来的后端了，要用我二次开发的这个。（其实也就是给用户信息表加了个字段））

### 白色主题（默认）

![](./public/白色主题.gif)

### 黑色主题

![](./public/黑色主题.gif)

### 蓝色主题

![](./public/蓝色主题.gif)

### 切换主题

#### 自由选择主题

![](./public/切换主题.gif)

#### 随机切换主题

![](./public/随机切换主题.gif)

### 在线测试

![](./public/在线测试.gif)

## Browser Support

Modern browsers and Internet Explorer 10+.

## LICENSE

[MIT](http://opensource.org/licenses/MIT)
